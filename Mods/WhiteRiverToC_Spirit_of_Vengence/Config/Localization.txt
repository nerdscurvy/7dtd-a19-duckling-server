Key,File,Type,UsedInMainMenu,NoTranslate,english,Context / Alternate Text,german,latam,french,italian,japanese,koreana,polish,brazilian,russian,turkish,schinese,tchinese,spanish
quest_spirit_vengeance_name,Quest,Quest Info,,,Elite Quest to obtain the Spirit of Vengeance Hell Beast,,,,,,,,,,,,,,
quest_spirit_vengeance_subtitle,Quest,Quest Info,,,Kill an evil demon,,,,,,,,,,,,,,
quest_spirit_vengeance_description,Quest,Quest Info,,,Help Johnny Blaze defeat an evil demon and obtain the Spirit of Vengeance's Hell Beast.,,,,,,,,,,,,,,
quest_spirit_vengeance_offer,Quest,Quest Info,,,"The Trader's friend Johnny Blaze needs some community help to defeat a Juggernaut demon. Help Mr. Blaze defeat the evil demon, and in return, obtain the Spirit of Vengeance's Hell Beast for your eternal enjoyment. The Trader suggests it may be a good idea to bring a friend with some extra first aid kits, and lots of ammo.",,,,,,,,,,,,,,
quest_spirit_vengeance_statement,Quest,Quest Info,,,Kill the evil demon,,,,,,,,,,,,,,
quest_spirit_vengeance_group,Quest,Quest Info,,,Spirit of Vengeance Hell Beast,,,,,,,,,,,,,,
questreward_spirit_vengeance_name,Quest,Quest Info,,,Elite Reward of a Hell Beast,,,,,,,,,,,,,,
questreward_spirit_vengeance_subtitle,Quest,Quest Info,,,Obtain a Hell Beast,,,,,,,,,,,,,,
questreward_spirit_vengeance_description,Quest,Quest Info,,,Return to the Trader to obtain the Spirit of Vengeance's Hell Beast.,,,,,,,,,,,,,,
questreward_spirit_vengeance_offer,Quest,Quest Info,,,It's said that the West was built on legends. And that legends are a way of understanding things greater than ourselves. Forces that shape our lives and events that defy explanation. Individuals whose lives soar to the heavens or fall to the earth. This is how legends are born. Return to the Trader to collect your Hell Beast.,,,,,,,,,,,,,,
questreward_spirit_vengeance_statement,Quest,Quest Info,,,Kill the evil demon,,,,,,,,,,,,,,
questreward_spirit_vengeance_group,Quest,Quest Info,,,Spirit of Vengeance Hell Beast,,,,,,,,,,,,,,
questitem_spirit_vengeance_name,items,Quest - Note,,,+Spirit of Vengeance Hell Beast Elite Quest,,,,,,,,,,,,,,
questitem_spirit_vengeance_description,items,Quest - Note,,,"Help Johnny Blaze kill an extremely tough demon to obtain a Hell Beast vehicle.\n\nYou get the distinct feeling you should bring lots of first aid kits, ammo, and maybe a friend.",,,,,,,,,,,,,,
questwrit_spirit_vengeance_name,items,Quest - Note,,,+Elite Writ of Spirit of Vengeance,,,,,,,,,,,,,,
questwrit_spirit_vengeance_description,items,Quest - Note,,,Evidence that you've bested the evil demon and deserve to be rewarded with a Hell Beast vehicle.,,,,,,,,,,,,,,
vehicle_spirit_hell_beast,vehicles,item,,,Hell Beast Motorcycle,,,,,,,,,,,,,,
vehicle_spirit_hell_beast_description,vehicles,item,,,A Hell Beast motorcycle you were rewarded for freeing Johnny Blaze from the Spirit of Vengeance.,,,,,,,,,,,,,,
medicalVengeanceFirstAidKit,items,Item,,,"First Aid Kit of Vengeance",,,,,,,,,,,,,,
medicalVengeanceFirstAidKitDesc,items,Item,,,"A First Aid Kit unlike anything you've seen before. A slow-acting heal over time for an insane amount of health.",,,,,,,,,,,,,,
quest_spiritstart_name,Quest,Quest Info,,,Elite Quest to obtain the Spirit of Vengeance Hell Beast,,,,,,,,,,,,,,
quest_spiritstart_subtitle,Quest,Quest Info,,,Kill an evil demon,,,,,,,,,,,,,,
quest_spiritstart_description,Quest,Quest Info,,,Help Johnny Blaze defeat an evil demon and obtain the Spirit of Vengeance's Hell Beast.,,,,,,,,,,,,,,
quest_spiritstart_statement,Quest,Quest Info,,,Kill the evil demon,,,,,,,,,,,,,,
quest_spiritstart_group,Quest,Quest Info,,,Spirit of Vengeance Hell Beast,,,,,,,,,,,,,,